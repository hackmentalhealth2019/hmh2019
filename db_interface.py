#!/usr/local/bin/python3

import sqlite3
import json
from http.server import BaseHTTPRequestHandler, HTTPServer

DB_NAME="main.db"
CLIENT_FEATURES = ["username TEXT UNIQUE", "age INT", "insurance TEXT", "nopocket INT", "yespocket INT", "budget INT", "hours INT", "mood INT", "anxiety INT", "focus INT", "pro INT"]
HELP_FEATURES = ["username TEXT UNIQUE", "first_name TEXT", "last_name TEXT", "age INT"]
INCL_FILTER_FEATURES = ["username", "col_name TEXT", "comparator TEXT", "value TEXT"]
EXCL_FILTER_FEATURES = ["username", "col_name TEXT", "comparator TEXT", "value TEXT"]


conn = None

def connect():
	global conn
	conn = sqlite3.connect('main.db')

def commit():
	conn.commit()

def close():
	conn.close()

def init():
	if conn == None:
		connect()
	cursor = conn.cursor()
	prologue = "CREATE TABLE IF NOT EXISTS "
	#create client tbl:
	cursor_string = prologue + "client (" + ", ".join(CLIENT_FEATURES) + ")"
	cursor.execute(cursor_string)
	#create help tbl:
	cursor_string = prologue + "help (" + ", ".join(HELP_FEATURES) + ")"
	cursor.execute(cursor_string)
	#create excl_filter tbl:
	cursor_string = prologue + "excl_filter (" + ", ".join(EXCL_FILTER_FEATURES) + ")"
	cursor.execute(cursor_string)
	#create incl_filter tbl:
	cursor_string = prologue + "incl_filter (" + ", ".join(INCL_FILTER_FEATURES) + ")"
	cursor.execute(cursor_string)
	conn.commit()

def surroundwithquotes(array):
	return map(lambda x: "'" + str(x) + "'", array)

def add_client(array):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "INSERT INTO client(" + ", ".join(map(lambda x: x.split()[0], CLIENT_FEATURES)) + ") VALUES(" + ", ".join(surroundwithquotes(array)) + ")"
	print(cursor_string)
	cursor.execute(cursor_string)
	conn.commit()

def retrieve_client(col_name, value):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT * from client WHERE " + col_name + "=\"" + value + "\""
	return cursor.execute(cursor_string).fetchall()

def retrieve_client_c(col_name, comparator, value):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT * from client WHERE " + col_name + comparator + value + "\""
	return cursor.execute(cursor_string).fetchall()

def add_help(array):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "INSERT INTO help(" + ", ".join(map(lambda x: x.split()[0], HELP_FEATURES)) + ") VALUES(" + ", ".join(surroundwithquotes(array)) + ")"
	cursor.execute(cursor_string)
	conn.commit()

def retrieve_help(col_name, value):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT * from help WHERE " + col_name + "=\"" + value + "\""
	return cursor.execute(cursor_string).fetchall()

def add_excl_filter(array):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "INSERT INTO excl_filter(" + ", ".join(map(lambda x: x.split()[0], EXCL_FILTER_FEATURES)) + ") VALUES(" + ", ".join(surroundwithquotes(array)) + ")"
	cursor.execute(cursor_string)
	conn.commit()

def add_incl_filter(array):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "INSERT INTO incl_filter(" + ", ".join(map(lambda x: x.split()[0], INCL_FILTER_FEATURES)) + ") VALUES(" + ", ".join(surroundwithquotes(array)) + ")"
	cursor.execute(cursor_string)
	conn.commit()

def retrieve_excl_filter(col_name, value):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT * from excl_filter WHERE " + col_name + "=\"" + value + "\""
	return cursor.execute(cursor_string).fetchall()

def retrieve_incl_filter(col_name, value):
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT * from incl_filter WHERE " + col_name + "=\"" + value + "\""
	return cursor.execute(cursor_string).fetchall()


def get_clients_for_help(fmtstr, username):
	excl_filters=retrieve_excl_filter("username", username)
	incl_filters=retrieve_incl_filter("username", username)
	if conn == None:
		connect()
	cursor = conn.cursor()
	cursor_string = "SELECT " + ", ".join(fmtstr) + " FROM client "
	if len(incl_filters) > 0 or len(excl_filters) > 0:
		cursor_string += "WHERE "
	for filter in incl_filters:
		cur_str = filter[1] + filter[2] + '"' + filter[3] + '"'
		cursor_string += cur_str + " AND "
		
	for filter in excl_filters:
		cur_str = " NOT " + filter[1] + filter[2] + '"' + filter[3] + '"'
		cursor_string += cur_str + " AND "

	if len(excl_filters) > 0 or len(incl_filters) > 0:
		cursor_string = cursor_string[:-len(" AND ")]

	print(cursor_string)
	return cursor.execute(cursor_string).fetchall()

class server_response(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Credentials', 'true')
        self.send_header('Access-Control-Allow-Origin', 'http://165.227.23.196')
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "X-Requested-With, Content-type")
        self.end_headers()
    def do_GET(self):
        self._set_headers()
        self.wfile.write("<html>OK</html".encode())
    def do_HEAD(self):
        self._set_headers()
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        if self.path == "/addefilter":
            self._set_headers()
            post_data = json.loads(self.rfile.read(content_length).decode())
            print(post_data)
            pl = post_data["payload"].split()
            add_excl_filter([post_data["name"], pl[0], pl[1],  pl[2]])
            self.wfile.write('{"response" : "Exclusive filter successfully added!"}'.encode())
        elif self.path == "/addifilter":
            self._set_headers()
            post_data = json.loads(self.rfile.read(content_length).decode())
            print(post_data)
            pl = post_data["payload"].split()
            add_incl_filter([post_data["name"], pl[0], pl[1],  pl[2]])
            self.wfile.write('{"response" : "Inclusive filter successfully added!"}'.encode())
        elif self.path == "/submit":
        	self._set_headers()
        	post_data = json.loads(self.rfile.read(content_length).decode())
        	print(post_data)
        	add_client([post_data["name"], post_data["age"], post_data["insurance"],  post_data["nopocket"], post_data["yespocket"], post_data["budget"], post_data["hours"], post_data["mood"], post_data["anxiety"], post_data["focus"], post_data["pro"]])
        	self.wfile.write('{"response" : "Profile successfully added!"}'.encode())
        elif self.path == "/getfilter":
            self._set_headers()
            post_data = json.loads(self.rfile.read(content_length).decode())
            print(post_data)
            excl_filters = retrieve_excl_filter("username", post_data["name"])
            incl_filters = retrieve_incl_filter("username", post_data["name"])
            html = "<table class='table is-bordered is-striped is-narrow is-hoverable is-fullwidth'><thead><tr><th>Exclusive Filters</th></tr></thead><tbody>"
            for f in excl_filters:
            	html += "<tr><td>" + f[1] + " " + f[2] + " " + f[3] + "</td></tr>"
            html += "</tbody><thead><tr><th>Inclusive Filters</th></tr></thead><tbody>"
            for f in incl_filters:
            	html += "<tr><td>" + f[1] + " " + f[2] + " " + f[3] + "</td></tr>"
            html += "</tbody></table>"
            response = '{"response" : "' + html + '"}'
            print(response)
            self.wfile.write(response.encode())
        elif self.path == "/getcli":
            self._set_headers()
            post_data = json.loads(self.rfile.read(content_length).decode())
            print(post_data)
            clients = get_clients_for_help(["username", "age", "insurance", "yespocket", "budget", "hours", "mood", "anxiety", "focus", "pro"], post_data["name"])
            #username TEXT UNIQUE", "age INT", "insurance TEXT", "nopocket INT", "yespocket INT", "budget INT", "hours INT", "mood INT", "anxiety INT", "focus INT", "pro INT
            html = "<table class='table is-bordered is-striped is-narrow is-hoverable is-fullwidth'><thead><tr><th>Name</th><th>Age</th><th>Insurance</th><th>Out of Pocket Pay</th><th>Budget</th><th>Hours</th><th>Mood</th><th>Anxiety</th><th>Focus</th><th>Procrastination</th></tr></thead><tbody>"
            for f in clients:
            	print(f)
            	html += "<tr><td>" + str(f[0]) + "</td><td>" + str(f[1]) + "</td><td>" + str(f[2]) + "</td><td>" + str(f[3]) + "</td><td>" + str(f[4]) + "</td><td>" + str(f[5]) + "</td><td>" + str(f[6]) + "</td><td>" + str(f[7]) + "</td><td>" + str(f[8]) + "</td><td>" + str(f[9]) + "</td></tr>"
            html += "</tbody></table>"
            response = '{"response" : "' + html + '"}'
            print(response)
            self.wfile.write(response.encode())
        else:
            self.wfile.write("Error: unhandeled path".encode())
        # self._set_headers()
def server():
	print("started")
	init()
	addr = ('', 8080)
	http_server = HTTPServer(addr, server_response)
	http_server.serve_forever()

server();



# init()
# add_client(["debrajsinha", "debraj", "sinha", "99", "50"])
# add_client(["chinmay", "chinmay", "sinha", "199", "20"])
# add_client(["michaelxu", "michael", "xu", "299", "40"])
# add_client(["molly", "debraj", "lll", "399", "60"])
# add_client(["aejf", "debraj", "sinha", "499", "80"])
# add_help(["diane", "diane", "diane", "999"])
# add_excl_filter(["diane", "age", ">", "200"])
# add_excl_filter(["diane", "username", "=", "michaelxu"])
# add_incl_filter(["diane", "budget", "<", "70"])
# print(retrieve_client("username","debrajsinha"))
# print(get_clients_for_help("diane"))